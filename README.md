# Prueba Técnica DevOps - Acme Bank

## Acceso a la aplicación

*  **Bank Web:** http://3.228.224.166:4000
 
*  **BackOffice:** http://3.228.224.166:4001/backoffice


## Prerequisitos de instalación

1.  Git
2.  Docker-ce 
3.  Docker-compose
4.  GitLab Runner


## Implementación

Para la implementación de este aplicación, se utilizó el servicio EC2 de AWS, creando una instancia y clonando el proyecto desde git:
    
<img src="https://gitlab.com/migu3lr/acme_bank_ex/raw/master/readme_assets/ec2.png">



Una vez creada la maquina EC2 a la cual se le debe asociar una Elastic IP para tener una IP pública fija, se debe proceder con la instalación de Git
    
    git clone https://gitlab.com/migu3lr/acme_bank_ex.git
    

Ademas de esto, se debe contar con Docker y docker-compose instalado.

Para utilizar el servicio de CI/CD de GitLab, se debe instalar tambien GitLab Runner configurando el ejecutor "Shell".


## Archivos

* acme_bank ->  Copia del repositorio https://github.com/wojtekmach/acme_bank
* .gitlab-ci.yml -> Archivo yaml de configuracion para CI/CD de Gitlab
* Dockerfile -> Archivo de configuracion para la imagen del servicio acme_bank
* deploy.sh -> Shell scritp para despliegue de la aplicación
* docker-compose.yml -> Archivo de configuracion para el conjunto de contenedores de la aplicacion

## CI/CD

Con el archivo de configuracion .gitlab-ci.yml, se ha creado un sistema para la automatizacion de las pruebas y despliegue de la aplicación.

<img src="https://gitlab.com/migu3lr/acme_bank_ex/raw/597715c5e7fc0afce6d3d76af1cbfbf238d975b7/readme_assets/deployment.png">