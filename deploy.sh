#!/bin/sh

#Build container images
docker-compose build

#Stopping and deleting previously created services
docker-compose down
docker volume prune --force

#Starting Postgres Service
docker-compose up -d postgres
docker-compose logs postgres

#Running ecto.setup for database population
docker-compose run phoenix mix ecto.setup

#Starting Phoenix (App) Service
docker-compose up -d phoenix
